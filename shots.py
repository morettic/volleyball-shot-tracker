import cairo
import random


BOX_PADDING = 0.05 # 5% of box
BOX_RESOLUTION = 0.05 # 5% of box
# Px_WHERE = [top-left-x, top-left-y, bottom-right-x, bottom-right-y]
P1_OPP =  [0,    0,    0.33, 2/6]
P2_OPP =  [0,    2/6,  0.33, 3/6]
P3_OPP =  [0.33, 2/6,  0.66, 3/6]
P4_OPP =  [0.66, 2/6,  1,    3/6]
P5_OPP =  [0.66, 0,    1,    2/6]
P6_OPP =  [0.33, 0,    0.66, 2/6]

P1_HOME = [0.66, 4/6, 1,    1  ]
P2_HOME = [0.66, 3/6, 1,    4/6]
P3_HOME = [0.33, 3/6, 0.66, 4/6]
P4_HOME = [0,    3/6, 0.33, 4/6]
P5_HOME = [0,    4/6, 0.33, 1  ]
P6_HOME = [0.33, 4/6, 0.66, 1  ]

def parse_shot(shot):
  shot_arr = []
  if len(shot) == 2:
    shot_arr += shot[0]
    shot_arr += shot[1]
    shot_arr += ' '
  elif len(shot) == 3:
    shot_arr += shot[0]
    shot_arr += shot[1]
    shot_arr += shot[2]
  else:
    raise ValueError('Invalid shot %s (%s)' % (shot, len(shot)))

  return shot_arr


def read_shots(shots_file):
  shots_array = []
  f = open(shots_file, 'r')
  for line in f:
      shots_array.append(parse_shot(line.strip()))

  return shots_array

def randomize_shot(x, y, padding):
  return '%.2f'%(random.uniform(x + padding, y - padding))


def get_shot_params(shot):
  """
  Return four coordinates and a cairo Pattern to draw a shot
  """
  shot_start = shot[0]
  shot_end = shot[1]
  shot_opts = shot[2]
  drawn_shot = []
  # Get the shot's x0 and y0
  if shot_start == "1":
    drawn_shot.append(randomize_shot(P1_HOME[0], P1_HOME[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P1_HOME[1], P1_HOME[3], BOX_PADDING))
  elif shot_start == "2":
    drawn_shot.append(randomize_shot(P2_HOME[0], P2_HOME[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P2_HOME[1], P2_HOME[3], BOX_PADDING))
  elif shot_start == "3":
    drawn_shot.append(randomize_shot(P3_HOME[0], P3_HOME[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P3_HOME[1], P3_HOME[3], BOX_PADDING))
  elif shot_start == "4":
    drawn_shot.append(randomize_shot(P4_HOME[0], P4_HOME[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P4_HOME[1], P4_HOME[3], BOX_PADDING))
  elif shot_start == "5":
    drawn_shot.append(randomize_shot(P5_HOME[0], P5_HOME[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P5_HOME[1], P5_HOME[3], BOX_PADDING))
  elif shot_start == "6":
    drawn_shot.append(randomize_shot(P6_HOME[0], P6_HOME[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P6_HOME[1], P6_HOME[3], BOX_PADDING))
  else:
    raise ValueError('Invalid start position %s for shot %s' % (shot_start, shot))

  # Get the shot's x1 and y1 (opponent trajectory)
  if shot_end == "1":
    drawn_shot.append(randomize_shot(P1_OPP[0], P1_OPP[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P1_OPP[1], P1_OPP[3], BOX_PADDING))
  elif shot_end == "2":
    drawn_shot.append(randomize_shot(P2_OPP[0], P2_OPP[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P2_OPP[1], P2_OPP[3], BOX_PADDING))
  elif shot_end == "3":
    drawn_shot.append(randomize_shot(P3_OPP[0], P3_OPP[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P3_OPP[1], P3_OPP[3], BOX_PADDING))
  elif shot_end == "4":
    drawn_shot.append(randomize_shot(P4_OPP[0], P4_OPP[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P4_OPP[1], P4_OPP[3], BOX_PADDING))
  elif shot_end == "5":
    drawn_shot.append(randomize_shot(P5_OPP[0], P5_OPP[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P5_OPP[1], P5_OPP[3], BOX_PADDING))
  elif shot_end == "6":
    drawn_shot.append(randomize_shot(P6_OPP[0], P6_OPP[2], BOX_PADDING))
    drawn_shot.append(randomize_shot(P6_OPP[1], P6_OPP[3], BOX_PADDING))
  else:
    raise ValueError('Invalid end position %s for shot %s' % (shot_end, shot))

  # Use shot option to get pattern
  #+-/*%
  if shot_opts == "+": # Strong shot
    drawn_shot.append(cairo.SolidPattern(0, 1, 0, 1)) # Green
  elif shot_opts == "-": # Weak Shot
    drawn_shot.append(cairo.SolidPattern(1, 0.73, 0, 1)) # Orange
  elif shot_opts == "*": # Libero shot
    drawn_shot.append(cairo.SolidPattern(1, 0, 0.85, 1)) # Purple
  elif shot_opts == "/": # Block
    drawn_shot.append(cairo.SolidPattern(0, 0, 1, 1)) # Blue
  elif shot_opts == "%": # Out
    drawn_shot.append(cairo.SolidPattern(1, 0, 0, 1)) # Red
  else:
    drawn_shot.append(cairo.SolidPattern(0, 0, 0, 1)) # Black

  return drawn_shot

def main():
  WIDTH, HEIGHT = 900, 1800 # 9m * 18m
  # Shot box is 1/3rd of the width (3 positions) and 1/4th of the height (two
  # player lines per side) minus padding
  #BOX_WIDTH, BOX_HEIGHT = WIDTH/3, HEIGHT/4

  # Initialize cairo drawing
  surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
  ctx = cairo.Context(surface)

  ctx.scale(WIDTH, HEIGHT)  # Normalizing the canvas

  # Draw the court
  solid_black = cairo.SolidPattern(0.0, 0.0, 0.0, 1)
  pat_home = cairo.SolidPattern(0.66, 0.66, 1, 1)
  pat_opp = cairo.SolidPattern(1, 0.66, 0.66, 1)
  ctx.rectangle(0, 0, 1, 1/2)  # Rectangle(x0, y0, x1, y1)
  ctx.set_source(pat_opp)
  ctx.fill()
  ctx.rectangle(0, 1/2, 1, 1)  # Rectangle(x0, y0, x1, y1)
  ctx.set_source(pat_home)
  ctx.fill()
  ctx.set_source(solid_black)
  ctx.move_to(0, 2/6)
  ctx.line_to(1, 2/6)  # Line to (x,y)
  ctx.move_to(0, 3/6)
  ctx.line_to(1, 3/6)  # Line to (x,y)
  ctx.move_to(0, 4/6)
  ctx.line_to(1, 4/6)  # Line to (x,y)
  ctx.set_line_width(0.01)
  ctx.stroke()

  # Get the shots
  shots = read_shots('shots_list.txt')

  # Draw each shot
  for shot in shots:
    params = get_shot_params(shot)
    ctx.move_to(float(params[0]), float(params[1]))
    ctx.line_to(float(params[2]), float(params[3]))
    ctx.set_source(params[4])
    #ctx.set_source(solid_black)
    ctx.set_line_width(0.005)
    ctx.stroke()

  surface.write_to_png("shots-color.png")  # Output to PNG

main()

#!/usr/bin/env python

import math
import cairo

WIDTH, HEIGHT = 900, 1800

surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
ctx = cairo.Context(surface)

ctx.scale(WIDTH, HEIGHT)  # Normalizing the canvas

#home_pat = cairo.LinearGradient(0.0, 0.0, 0.0, 1.0)
#pat.add_color_stop_rgba(1, 0.7, 0, 0, 0.5)  # First stop, 50% opacity
#pat.add_color_stop_rgba(0, 0.9, 0.7, 0.2, 1)  # Last stop, 100% opacity

solid_black = cairo.SolidPattern(0.0, 0.0, 0.0, 1)
pat_home = cairo.SolidPattern(0.0, 0.0, 1, 0.3)
pat_opp = cairo.SolidPattern(1, 0.0, 0.0, 0.3)
ctx.rectangle(0, 0, 1, 1/2)  # Rectangle(x0, y0, x1, y1)
ctx.set_source(pat_opp)
ctx.fill()
ctx.rectangle(0, 1/2, 1, 1)  # Rectangle(x0, y0, x1, y1)
ctx.set_source(pat_home)
ctx.fill()
ctx.set_source(solid_black)
ctx.move_to(0, 2/6)
ctx.line_to(1, 2/6)  # Line to (x,y)
ctx.move_to(0, 3/6)
ctx.line_to(1, 3/6)  # Line to (x,y)
ctx.move_to(0, 4/6)
ctx.line_to(1, 4/6)  # Line to (x,y)
ctx.set_line_width(0.01)
ctx.stroke()

ctx.move_to(0,0)
ctx.line_to(1,1)
ctx.set_source(cairo.SolidPattern(1,0,0,1))
ctx.set_line_width(0.002)
ctx.stroke()

surface.write_to_png("example.png")  # Output to PNG
